cmake_minimum_required(VERSION 3.14)

add_executable(aoc09
    main.cpp
)

target_compile_options(aoc09
    PRIVATE
    -Wall -Wextra
    -Wshadow -Wnon-virtual-dtor -Wold-style-cast
    -Woverloaded-virtual -Wzero-as-null-pointer-constant
    -pedantic -fno-rtti
)

set_property(TARGET aoc09 PROPERTY CXX_STANDARD 17)

add_custom_command(
	TARGET aoc09 POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy
			${CMAKE_SOURCE_DIR}/09/input09.txt
			${CMAKE_BINARY_DIR}/input09.txt
)