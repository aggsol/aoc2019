#include <bits/stdint-intn.h>
#include <bits/stdint-uintn.h>
#include <cassert>
#include <cstdint>
#include <map>
#include <memory>
#include <iostream>
#include <stdexcept>
#include <string>
#include <deque>
#include <vector>
#include <sstream>
#include <fstream>
#include <limits>

class IntCoder
{
	int POSITION_MODE = 0;
	int INTERMEDIATE_MODE = 1;
	int RELATIVE_MODE = 2;
public:
	IntCoder() = default;

	void store(int64_t addr, std::int64_t val, int mode)	
	{
		assert(mode != INTERMEDIATE_MODE);
		if(mode == POSITION_MODE)
		{
			assert(addr >= 0);
			memory[addr] = val;
			return;
		}
		if(mode == RELATIVE_MODE)
		{
			assert(relativeBase+addr >= 0);
			memory[relativeBase+addr] = val;
			return;
		}
		throw std::runtime_error("STORE invalid mode");
	}

	int64_t load(int64_t instrPtr, int64_t mode)
	{
		assert(instrPtr >= 0);		
		if(mode == POSITION_MODE)
		{ 
			auto addr = memory[instrPtr];
			return memory[addr];
		}
		if(mode == INTERMEDIATE_MODE) 
		{ 
			return memory[instrPtr];
		}
		if(mode == RELATIVE_MODE)
		{
			std::cout << "LOAD_REL instrPtr=" << instrPtr << " mode=" << mode
				<< " relBase=" << relativeBase;
			auto offset = memory[instrPtr];
			std::cout << " offset=" << offset << "\n";
			assert(relativeBase+offset >= 0);

			return memory[relativeBase+offset];
		} 

		throw std::runtime_error("unknown mode");
	}

	void parse(std::istream& is)
	{
		char c;
		std::string integer;
		uint64_t index = 0;
		while(is.get(c))
		{			
			if(c == ',')
			{
				assert(not integer.empty());
				auto val = std::stoull(integer);
				memory[index++] = val;
				integer.clear();
			}
			else
			{
				integer += c;
			}
		}

		if(not integer.empty())
		{
			auto val = std::stoi(integer);
			memory[index++] = val;
		}
	}

	void process()
	{
		bool isProcessing = true;
		int64_t instrPtr = 0;
		std::string input;
		while(isProcessing)
		{
			int64_t instr = load(instrPtr, INTERMEDIATE_MODE);
			int64_t opcode = instr % 100ull;
			int64_t mode1 = (instr / 100ull) % 10ull;
			int64_t mode2 = (instr / 1000ull) % 10ull;
			int64_t mode3 = (instr / 10000ull) % 10ull;

			if(opcode == 1) // add
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);

				instrPtr++;
				auto b = load(instrPtr, mode2);
				
				instrPtr++;
				auto dest = memory[instrPtr];

				// std::cout << "ADD a=" << a << " mode1=" << mode1
				// << " b=" << b << " mode2=" << mode2
				// << " dest=" << dest  << " mode=" << mode3
				// << "\n";

				store(dest, a + b, mode3);
				instrPtr++;
			}
			else if(opcode == 2) // mul
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				
				instrPtr++;
				auto b = load(instrPtr, mode2);
				
				instrPtr++;
				auto dest = memory[instrPtr];

				// std::cout << "MUL a=" << a << " mode1=" << mode1
				// << " b=" << b << " mode2=" << mode2
				// << " dest=" << dest  << " mode=" << mode3
				// << "\n";

				store(dest, a * b, mode3);
				instrPtr++;
			}
			else if(opcode == 3) // input
			{
				int64_t val = -1;
				input.clear();
				std::cout << "input : ";
				getline (std::cin, input);
				std::stringstream(input) >> val;
				
				instrPtr++;
				auto a = memory[instrPtr];
				std::cout << "INP val=" << val << " a=" << a << " mode1=" << mode1 << "\n";
				store(a, val, mode1);			
				instrPtr++;
			}
			else if(opcode == 4) // ouput
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				std::cout << "output: " << a << "\n";
				instrPtr++;
			}
			else if(opcode == 5) // jump-if-true
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);

				instrPtr++;
				if(a != 0)
				{
					auto b = load(instrPtr, mode2);
					instrPtr = b;
				}
				else
				{
					instrPtr++;	
				}
			}
			else if(opcode == 6) // jump-if-false
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);

				instrPtr++;
				if(a == 0)
				{
					auto b = load(instrPtr, mode2);
					instrPtr = b;
				}
				else
				{
					instrPtr++;	
				}
			}
			else if(opcode == 7) // less than
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				
				instrPtr++;
				auto b = load(instrPtr, mode2);
				
				instrPtr++;
				auto dest = memory[instrPtr];

				if(a < b)
				{
					store(dest, 1, mode3);
				}
				else 
				{
					store(dest, 0, mode3);
				}
				instrPtr++;
			}			
			else if(opcode == 8) // equals
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				
				instrPtr++;
				auto b = load(instrPtr, mode2);
				
				instrPtr++;
				auto dest = memory[instrPtr];

				if(a == b)
				{
					store(dest, 1, mode3);
				}
				else 
				{
					store(dest, 0, mode3);
				}
				instrPtr++;
			}
			else if(opcode == 9) // adjust relative base
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				relativeBase += a;
				assert(relativeBase >= 0);
				std::cout << "ARB      a=" << a << " relBase=" << relativeBase << " instrPtr=" << instrPtr << "\n";
				instrPtr++;
			}
			else if(opcode == 99) // halt
			{
				isProcessing = false;
			}
			else
			{
				std::ostringstream msg;
				msg << "invalid opcode=" << opcode << "\n";
				throw std::runtime_error(msg.str());
				return;
			}
		}
	}

	std::map<int64_t, int64_t>	memory;
	int64_t relativeBase = 0;
};

void test01();
void test02();
void inputTest();
void test04();
void test05();
void run();

int main()
{
	assert(1125899906842624ull < std::numeric_limits<int64_t>::max());
	// test01();
	// test02();
	// inputTest();
	// test04();
	// test05();
	run();
		
	return 0;
}

void run()
{
	std::ifstream input("input09.txt");

	if(not input.is_open())
	{
		throw std::runtime_error("cannot open file");
	}

	IntCoder intcoder;

	intcoder.parse(input);
	intcoder.process();

	std::cout << "done\n";
}

void test05()
{
	std::cout << "- start test05\n";
	{
		std::stringstream data;
		data << "104,1125899906842624,99";

		IntCoder intcoder;
		intcoder.parse(data);
		intcoder.process();

	}
	{
		std::stringstream data;
		data << "109,19,204,-34,99";

		IntCoder intcoder;
		intcoder.parse(data);
		intcoder.relativeBase = 2000;
		intcoder.process();

		assert(intcoder.relativeBase == 2019);
		assert(intcoder.memory.at(1985) == 0);
	}
	std::cout << "- test05 done\n";
}

void test04()
{
	std::cout << "- start test04\n";
	std::stringstream data;
	data << "1002,4,3,4,33";

	IntCoder intcoder;
	intcoder.parse(data);
	intcoder.process();

	assert(intcoder.memory.size() == 5);

	assert(intcoder.memory[0] == 1002);
	assert(intcoder.memory[1] == 4);
	assert(intcoder.memory[2] == 3);
	assert(intcoder.memory[3] == 4);
	assert(intcoder.memory[4] == 99);

	std::cout << "- test04 done\n";
}

void inputTest()
{ 
	std::cout << "- start inputTest\n";
	{
		std::stringstream data;
		data << "3,3,1105,-1,9,1101,0,0,12,4,12,99,1";

		IntCoder intcoder;
		intcoder.parse(data);
		intcoder.process();
	}
	{
		std::stringstream data;
		data << "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99";

		IntCoder intcoder;
		intcoder.parse(data);
		intcoder.process();
	}
	std::cout << "- inputTest done\n";
}

void test02()
{
	std::cout << "- start test02\n";
	{
		std::stringstream data;
		data << "2,3,0,3,99";

		IntCoder intcoder;
		intcoder.parse(data);
		intcoder.process();

		assert(intcoder.memory.size() == 5);

		assert(intcoder.memory[0] == 2);
		assert(intcoder.memory[1] == 3);
		assert(intcoder.memory[2] == 0);
		assert(intcoder.memory[3] == 6);
		assert(intcoder.memory[4] == 99);
	}
	{
		std::stringstream data;
		data << "2,4,4,5,99,0";

		IntCoder intcoder;
		intcoder.parse(data);
		intcoder.process();

		assert(intcoder.memory.size() == 6);

		assert(intcoder.memory[0] == 2);
		assert(intcoder.memory[1] == 4);
		assert(intcoder.memory[2] == 4);
		assert(intcoder.memory[3] == 5);
		assert(intcoder.memory[4] == 99);
		assert(intcoder.memory[5] == 9801);
	}	
	{
		std::stringstream data;
		data << "1,1,1,4,99,5,6,0,99";

		IntCoder intcoder;
		intcoder.parse(data);
		intcoder.process();

		assert(intcoder.memory.size() == 9);

		assert(intcoder.memory[0] == 30);
		assert(intcoder.memory[1] == 1);
		assert(intcoder.memory[2] == 1);
		assert(intcoder.memory[3] == 4);
		assert(intcoder.memory[4] == 2);
		assert(intcoder.memory[5] == 5);
		assert(intcoder.memory[6] == 6);
		assert(intcoder.memory[7] == 0);
		assert(intcoder.memory[8] == 99);
	}
	std::cout << "- test02 done\n";
}

void test01()
{ 
	std::cout << "- start test01\n";
	std::stringstream data;
	data << "1,0,0,0,99";

	IntCoder intcoder;
	intcoder.parse(data);

	assert(intcoder.memory.size() == 5);

	assert(intcoder.memory[0] == 1);
	assert(intcoder.memory[1] == 0);
	assert(intcoder.memory[2] == 0);
	assert(intcoder.memory[3] == 0);
	assert(intcoder.memory[4] == 99);

	intcoder.process();

	assert(intcoder.memory[0] == 2);
	std::cout << "- test01 done\n";
}