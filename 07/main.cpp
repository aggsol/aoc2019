#include <bits/stdint-intn.h>
#include <bits/stdint-uintn.h>
#include <cassert>
#include <cstdint>
#include <map>
#include <memory>
#include <iostream>
#include <stdexcept>
#include <string>
#include <deque>
#include <vector>
#include <sstream>
#include <fstream>
#include <limits>

class IntCoder
{
	int POSITION_MODE = 0;
	int INTERMEDIATE_MODE = 1;
	int RELATIVE_MODE = 2;
public:
	IntCoder(int64_t phase, int64_t signal)
	{
		inputs.push_back(phase);
		inputs.push_back(signal);

		std::cout << "phase=" << phase << " signal=" << signal << "\n";
	} 

	void store(int64_t addr, std::int64_t val, int mode)	
	{
		assert(mode != INTERMEDIATE_MODE);
		if(mode == POSITION_MODE)
		{
			assert(addr >= 0);
			memory[addr] = val;
			return;
		}
		if(mode == RELATIVE_MODE)
		{
			assert(relativeBase+addr >= 0);
			memory[relativeBase+addr] = val;
			return;
		}
		throw std::runtime_error("STORE invalid mode");
	}

	int64_t load(int64_t instrPtr, int64_t mode)
	{
		assert(instrPtr >= 0);		
		if(mode == POSITION_MODE)
		{ 
			auto addr = memory[instrPtr];
			return memory[addr];
		}
		if(mode == INTERMEDIATE_MODE) 
		{ 
			return memory[instrPtr];
		}
		if(mode == RELATIVE_MODE)
		{
			std::cout << "LOAD_REL instrPtr=" << instrPtr << " mode=" << mode
				<< " relBase=" << relativeBase;
			auto offset = memory[instrPtr];
			std::cout << " offset=" << offset << "\n";
			assert(relativeBase+offset >= 0);

			return memory[relativeBase+offset];
		} 

		throw std::runtime_error("unknown mode");
	}

	void parse(std::istream& is)
	{
		char c;
		std::string integer;
		uint64_t index = 0;
		while(is.get(c))
		{			
			if(c == ',')
			{
				assert(not integer.empty());
				auto val = std::stoull(integer);
				memory[index++] = val;
				integer.clear();
			}
			else
			{
				integer += c;
			}
		}

		if(not integer.empty())
		{
			auto val = std::stoi(integer);
			memory[index++] = val;
		}
	}

	void process()
	{
		bool isProcessing = true;
		int64_t instrPtr = 0;
		std::string input;
		while(isProcessing)
		{
			int64_t instr = load(instrPtr, INTERMEDIATE_MODE);
			int64_t opcode = instr % 100ull;
			int64_t mode1 = (instr / 100ull) % 10ull;
			int64_t mode2 = (instr / 1000ull) % 10ull;
			int64_t mode3 = (instr / 10000ull) % 10ull;

			if(opcode == 1) // add
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);

				instrPtr++;
				auto b = load(instrPtr, mode2);
				
				instrPtr++;
				auto dest = memory[instrPtr];

				// std::cout << "ADD a=" << a << " mode1=" << mode1
				// << " b=" << b << " mode2=" << mode2
				// << " dest=" << dest  << " mode=" << mode3
				// << "\n";

				store(dest, a + b, mode3);
				instrPtr++;
			}
			else if(opcode == 2) // mul
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				
				instrPtr++;
				auto b = load(instrPtr, mode2);
				
				instrPtr++;
				auto dest = memory[instrPtr];

				// std::cout << "MUL a=" << a << " mode1=" << mode1
				// << " b=" << b << " mode2=" << mode2
				// << " dest=" << dest  << " mode=" << mode3
				// << "\n";

				store(dest, a * b, mode3);
				instrPtr++;
			}
			else if(opcode == 3) // input
			{
				int64_t val = inputs.at(inputIndex);
				
				instrPtr++;
				auto a = memory[instrPtr];
				// std::cout << "INP val=" << val << " a=" << a << " mode1=" << mode1 << "\n";
				store(a, val, mode1);			
				instrPtr++;
			}
			else if(opcode == 4) // ouput
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				// std::cout << "OUT: " << a << "\n";
				output = a;
				instrPtr++;
			}
			else if(opcode == 5) // jump-if-true
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);

				instrPtr++;
				if(a != 0)
				{
					auto b = load(instrPtr, mode2);
					instrPtr = b;
				}
				else
				{
					instrPtr++;	
				}
			}
			else if(opcode == 6) // jump-if-false
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);

				instrPtr++;
				if(a == 0)
				{
					auto b = load(instrPtr, mode2);
					instrPtr = b;
				}
				else
				{
					instrPtr++;	
				}
			}
			else if(opcode == 7) // less than
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				
				instrPtr++;
				auto b = load(instrPtr, mode2);
				
				instrPtr++;
				auto dest = memory[instrPtr];

				if(a < b)
				{
					store(dest, 1, mode3);
				}
				else 
				{
					store(dest, 0, mode3);
				}
				instrPtr++;
			}			
			else if(opcode == 8) // equals
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				
				instrPtr++;
				auto b = load(instrPtr, mode2);
				
				instrPtr++;
				auto dest = memory[instrPtr];

				if(a == b)
				{
					store(dest, 1, mode3);
				}
				else 
				{
					store(dest, 0, mode3);
				}
				instrPtr++;
			}
			else if(opcode == 9) // adjust relative base
			{
				instrPtr++;
				auto a = load(instrPtr, mode1);
				relativeBase += a;
				assert(relativeBase >= 0);
				std::cout << "ARB      a=" << a << " relBase=" << relativeBase << " instrPtr=" << instrPtr << "\n";
				instrPtr++;
			}
			else if(opcode == 99) // halt
			{
				isProcessing = false;
			}
			else
			{
				std::ostringstream msg;
				msg << "invalid opcode=" << opcode << "\n";
				throw std::runtime_error(msg.str());
				return;
			}
		}
	}

	std::map<int64_t, int64_t>	memory;
	int64_t relativeBase = 0;
	std::vector<int64_t>		inputs;
	unsigned					inputIndex = 0;
	int64_t						output = -1;
};

int64_t run(int64_t phase, int64_t signal);

int64_t runAmps(int64_t p1, int64_t p2, int64_t p3, int64_t p4, int64_t p5)
{
	auto ampA = run(p1, 4);
	auto ampB = run(p2, ampA);
	auto ampC = run(p3, ampB);
	auto ampD = run(p4, ampC);
	auto ampE = run(p5, ampD);
	return ampE;
}

int main()
{
	assert(1125899906842624ull < std::numeric_limits<int64_t>::max());

	std::stringstream in;
	in << "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0";
	IntCoder intcoder(0, 0);

	intcoder.parse(in);
	intcoder.process();
	std::cout << intcoder.output << "\n";

	auto result = runAmps(4,3,2,1,0);
	std:: cout << "4,3,2,1,0=" << result << "\n";

	// int64_t max = 0;
	// for(int p1=0; p1<5; ++p1)
	// {
	// for(int p2=0; p2<5; ++p2)
	// {
	// for(int p3=0; p3<5; ++p3)
	// {
	// for(int p4=0; p4<5; ++p4)
	// {
	// for(int p5=0; p5<5; ++p5)
	// {
	// 	auto val = runAmps(p1, p2, p3, p4, p5);
	// 	if(val > max)
	// 	{
	// 		max = val;
	// 	}
	// }		
	// }	
	// }		
	// }
	// }
	// std::cout << "max thrusters=" << max << "\n";
}

int64_t run(int64_t phase, int64_t signal)
{
	std::ifstream input("input07.txt");

	if(not input.is_open())
	{
		throw std::runtime_error("cannot open file");
	}

	IntCoder intcoder(phase, signal);

	intcoder.parse(input);
	intcoder.process();

	return intcoder.output;
}

