#include <iostream>
#include <fstream>
#include <cassert>
#include <string>

long calcFuel(long mass)
{
	return (mass / 3) - 2;
}

long calcFuelComplete(long mass)
{
	long result = 0;

	auto fuel = calcFuel(mass);
	while(fuel > 0)
	{
		result += fuel;
		fuel = calcFuel(fuel);
	}

	return result;
}

int main()
{
	assert(calcFuel(12) == 2);
	assert(calcFuel(14) == 2);
	assert(calcFuel(1969) == 654);
	assert(calcFuel(100756) == 33583);

	assert(calcFuelComplete(14) == 2);
	assert(calcFuelComplete(1969) == 966);
	assert(calcFuelComplete(100756) == 50346);

	std::ifstream input("input01.txt");

	if(not input.is_open())
	{
		std::cerr << "cannot open file.";
		return 100;
	}

	std::string line;
	long result01 = 0;
	long result02 = 0;
	long sumMass;
	unsigned module = 1;
	while(input)
	{
		std::getline(input, line);
		if(line.empty()) continue;
		auto mass = std::stol(line);
		sumMass += mass;
		//std::cout << "module " << module << " mass" << " :" << mass << "\n";
		result01 += calcFuel(mass);
		result02 += calcFuelComplete(mass);

		module++;
	}
	std::cout << "fuel for modules: " << result01 << "\n";
	std::cout << "complete fuel requirement: " << result02 << "\n";

	return 0;
}