#include <cassert>
#include <memory>
#include <iostream>
#include <stdexcept>
#include <string>
#include <deque>
#include <vector>
#include <sstream>
#include <fstream>

class IntCoder
{
	int POSITION_MODE = 0;
	int INTERMEDIATE_MODE = 1;
public:
	IntCoder() = default;

	void parse(std::istream& is)
	{
		char c;
		std::string integer;
		while(is.get(c))
		{			
			if(c == ',')
			{
				assert(not integer.empty());
				auto val = std::stoi(integer);
				memory.push_back(val);
				integer.clear();
			}
			else
			{
				integer += c;
			}
		}

		if(not integer.empty())
		{
			auto val = std::stoi(integer);
			memory.push_back(val);
		}
	}

	void process()
	{
		bool isProcessing = true;
		int instrPtr = 0;
		std::string input;
		while(isProcessing)
		{
			int instr = memory.at(instrPtr);
			int opcode = instr % 100;
			int mode1 = (instr / 100) % 10;
			int mode2 = (instr / 1000) % 10;
			int mode3 = (instr / 10000) % 10;

			if(opcode == 1) // add
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				if(mode1 == POSITION_MODE) a = memory.at(a);

				instrPtr++;
				auto b = memory.at(instrPtr);
				if(mode2 == POSITION_MODE) b = memory.at(b);
				
				instrPtr++;
				auto dest = memory.at(instrPtr);

				memory.at(dest) = a + b;
				instrPtr++;
			}
			else if(opcode == 2) // mul
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				if(mode1 == POSITION_MODE) a = memory.at(a);
				
				instrPtr++;
				auto b = memory.at(instrPtr);
				if(mode2 == POSITION_MODE) b = memory.at(b);
				
				instrPtr++;
				auto dest = memory.at(instrPtr);
				assert(mode3 == POSITION_MODE);

				// std::cout << "mul a=" << a << " mode1=" << mode1
				// << " b=" << b << " mode2=" << mode2
				// << " dest=" << dest  << " mode=" << mode3
				// << "\n";

				memory.at(dest) = a * b;
				instrPtr++;
			}
			else if(opcode == 3) // input
			{
				int val = -1;
				input.clear();
				std::cout << "input : ";
				getline (std::cin, input);
				std::stringstream(input) >> val;
				
				instrPtr++;
				auto a = memory.at(instrPtr);
				//std::cout << "val=" << val << " a=" << a << "\n";
				memory.at(a) = val;
				instrPtr++;
			}
			else if(opcode == 4) // ouput
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				if(mode1 == POSITION_MODE) a = memory.at(a);
				std::cout << "output: " << a << "\n";
				instrPtr++;
			}
			else if(opcode == 5) // jump-if-true
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				if(mode1 == POSITION_MODE) a = memory.at(a);

				instrPtr++;
				if(a != 0)
				{
					auto b = memory.at(instrPtr);
					if(mode2 == 0) b = memory.at(b);
					instrPtr = b;
				}
				else
				{
					instrPtr++;	
				}
			}
			else if(opcode == 6) // jump-if-false
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				if(mode1 == POSITION_MODE) a = memory.at(a);

				instrPtr++;
				if(a == 0)
				{
					auto b = memory.at(instrPtr);
					if(mode2 == 0) b = memory.at(b);
					instrPtr = b;
				}
				else
				{
					instrPtr++;	
				}
			}
			else if(opcode == 7) // less than
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				if(mode1 == POSITION_MODE) a = memory.at(a);
				
				instrPtr++;
				auto b = memory.at(instrPtr);
				if(mode2 == POSITION_MODE) b = memory.at(b);
				
				instrPtr++;
				auto dest = memory.at(instrPtr);
				assert(mode3 == POSITION_MODE);

				if(a < b)
				{
					memory.at(dest) = 1;
				}
				else 
				{
					memory.at(dest) = 0;
				}
				instrPtr++;
			}			
			else if(opcode == 8) // equals
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				if(mode1 == POSITION_MODE) a = memory.at(a);
				
				instrPtr++;
				auto b = memory.at(instrPtr);
				if(mode2 == POSITION_MODE) b = memory.at(b);
				
				instrPtr++;
				auto dest = memory.at(instrPtr);
				assert(mode3 == POSITION_MODE);

				if(a == b)
				{
					memory.at(dest) = 1;
				}
				else 
				{
					memory.at(dest) = 0;
				}
				instrPtr++;
			}
			else if(opcode == 99) // halt
			{
				isProcessing = false;
			}
			else
			{
				std::cerr << "invalid opcode=" << opcode << "\n";
				return;
			}
		}
	}

	std::vector<int>	memory;
};

void test01();
void test02();
void test03();
void test04();
void run();

int main()
{
	//test01();
	//test02();
	//test03();
	//test04();
	run();
		
	return 0;
}

void run()
{
	std::ifstream input("input05.txt");

	if(not input.is_open())
	{
		throw std::runtime_error("cannot open file");
	}

	IntCoder intcoder;

	intcoder.parse(input);
	intcoder.process();

	std::cout << "done\n";
}

void test04()
{
	std::stringstream data;
	data << "1002,4,3,4,33";

	IntCoder intcoder;
	intcoder.parse(data);
	intcoder.process();

	assert(intcoder.memory.size() == 5);

	assert(intcoder.memory[0] == 1002);
	assert(intcoder.memory[1] == 4);
	assert(intcoder.memory[2] == 3);
	assert(intcoder.memory[3] == 4);
	assert(intcoder.memory[4] == 99);

	std::cout << "test04 done\n";
}

void test03()
{
	std::stringstream data;
	data << "3,0,4,0,99";

	IntCoder intcoder;
	intcoder.parse(data);
	intcoder.process();

	std::cout << "test03 done\n";
}

void test02()
{
	std::stringstream data;
	data << "2,3,0,3,99";

	IntCoder intcoder;
	intcoder.parse(data);
	intcoder.process();

	assert(intcoder.memory.size() == 5);

	assert(intcoder.memory[0] == 2);
	assert(intcoder.memory[1] == 3);
	assert(intcoder.memory[2] == 0);
	assert(intcoder.memory[3] == 6);
	assert(intcoder.memory[4] == 99);

	std::cout << "test02 done\n";
}

void test01()
{
	std::stringstream data;
	data << "1,0,0,0,99";

	IntCoder intcoder;
	intcoder.parse(data);

	assert(intcoder.memory.size() == 5);

	assert(intcoder.memory[0] == 1);
	assert(intcoder.memory[1] == 0);
	assert(intcoder.memory[2] == 0);
	assert(intcoder.memory[3] == 0);
	assert(intcoder.memory[4] == 99);

	intcoder.process();

	assert(intcoder.memory[0] == 2);
	std::cout << "test01 done\n";
}