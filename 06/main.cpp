#include <cassert>
#include <cstdint>
#include <memory>
#include <iostream>
#include <string>
#include <deque>
#include <map>
#include <sstream>
#include <fstream>

class SpaceObject
{
public:
	SpaceObject() = default;
	std::string		Name;
	unsigned		OrbiterCount = 0;

	void addChild(std::shared_ptr<SpaceObject> c)
	{
		//std::cout << "add " << c->Name << " to " << Name << "\n";
		for(auto curr: m_children)
		{
			assert(curr->Name != c->Name);
		}
		auto count = c->getOrbiterCount() + 1;
		OrbiterCount += count;
		if(not m_parent.expired()) m_parent.lock()->incOrbiterCount(count);

		m_children.push_back(c);
	}

	std::weak_ptr<SpaceObject> parent() const {return m_parent; }
	void parent(std::weak_ptr<SpaceObject> p) {m_parent = p;}

	const std::deque<std::shared_ptr<SpaceObject>>& children() const { return m_children; }

	unsigned getChildDepth(const std::string& name)
	{
		unsigned result = 0;

		for(auto c: m_children)
		{
			if(c->Name == name) 
			{ 
				std::cout << Name << " ) " << name << "\n";
				return 1;				
			}
			result = c->getChildDepth(name);
			if(result != 0) 
			{ 
				std::cout << Name << " - " << name << " depth=" << result << "\n";
				break; 
			}
		}

		return result;
	}

private:
	unsigned getOrbiterCount() const
	{		
		unsigned result = m_children.size();		
		for(auto c: m_children)
		{
			result += c->getOrbiterCount();
		}
		//std::cout << Name << ": orbiter count=" << result << "\n";
		return result;
	}

	void incOrbiterCount(unsigned val)
	{
		OrbiterCount += val;
		if(not m_parent.expired()) m_parent.lock()->incOrbiterCount(val);
	}

	std::weak_ptr<SpaceObject>					m_parent;
	std::deque<std::shared_ptr<SpaceObject>>	m_children;
};

class OrbitMap
{
public:
	void parseMap(std::istream& map)
	{
		std::string line;
		std::string center;
		std::string orbiter;
		
		while(map)
		{
			std::getline(map, line);			 

			if(line.empty()) continue;

			auto pos = line.find(")");
			assert(pos != std::string::npos);
			center = line.substr(0, pos);
			orbiter = line.substr(pos+1);

			auto co = get(center);
			auto oo = get(orbiter);

			assert(oo->parent().use_count() == 0);
			oo->parent(co);
			co->addChild(oo);
		}
	}

	unsigned calcOrbiterCountChecksum() const
	{
		unsigned result = 0;
		for(auto o: m_objects)
		{
			result += o.second->OrbiterCount;
		}
		return result;
	}

	unsigned numObjects() const { return m_objects.size(); }

	int calcNumTransfers(const std::string& from, const std::string& to)
	{
		int ups = 0;
		int downs = 0;
		bool found = false;

		auto start = m_objects.at(from)->parent().lock();
		assert(start != nullptr);

		auto dest = m_objects.at(to)->parent().lock();
		assert(dest != nullptr);

		while(start != nullptr && found == false)
		{
			if(start->Name == dest->Name)
			{
				found = true;
				break;
			}
			for(auto c : start->children())
			{
				assert(c != nullptr);
				downs = c->getChildDepth(dest->Name);
				if(downs != 0)
				{ 
					found = true;
					break; 
				}
			}
			//std::cout << dest->Name << " is no child of " << start->Name << "\n";
			start = start->parent().lock();
			ups++;
		}
		if(not found) return -1;
		std::cout << "ups=" << ups << " downs=" << downs << "\n";
		return ups-downs;
	}

private:
	std::shared_ptr<SpaceObject> get(const std::string& name)
	{
		auto it = m_objects.find(name);
		if(it == m_objects.end())
		{
			auto co = std::make_shared<SpaceObject>();				
			co->Name = name;
			m_objects[name] = co;
		}
		else if(false)
		{
			std::cout << name << " already exists";
			auto parent =  m_objects[name]->parent().lock();
			if(parent)
			{
				std::cout << " parent=" << parent->Name;
			} 
			std::cout << "\n";
		}
		return m_objects[name];
	}
	
	std::map<std::string, std::shared_ptr<SpaceObject>>	m_objects;
};

void test01();
void part01();
void test02();
void part02();

int main()
{
	test01();
	part01();
	test02();
	part02();
	return 0;
}

void part02()
{
	std::ifstream file("input06.txt");
	if(not file.is_open()) throw std::runtime_error("cannot open file");

	OrbitMap orbitMap;
	orbitMap.parseMap(file);
	auto transfers = orbitMap.calcNumTransfers("YOU", "SAN");
	std::cout << "part02: " << transfers << "\n";
}

void test02()
{
	std::stringstream mapData;
	mapData << "COM)B\n"
			<< "B)C\n"
			<< "C)D\n"
			<< "D)E\n"
			<< "E)F\n"
			<< "B)G\n"
			<< "G)H\n"
			<< "D)I\n"
			<< "E)J\n"
			<< "J)K\n"
			<< "K)L\n"
			<< "K)YOU\n"
			<< "I)SAN\n";

	OrbitMap orbitMap;
	orbitMap.parseMap(mapData);
	std::cout << "test02: numObjects=" << orbitMap.numObjects() << "\n";
	//auto checksum = orbitMap.calcOrbiterCountChecksum();
	auto transfers = orbitMap.calcNumTransfers("YOU", "SAN");
	std::cout << "test02: " << transfers << "\n";
}

void part01()
{
	std::ifstream file("input06.txt");
	if(not file.is_open()) throw std::runtime_error("cannot open file");

	OrbitMap orbitMap;
	orbitMap.parseMap(file);
	std::cout << "part01: numObjects=" << orbitMap.numObjects() << "\n";
	std::cout << "part01: checksum=" << orbitMap.calcOrbiterCountChecksum() << "\n";
}

void test01()
{
	std::stringstream mapData;
	mapData << "COM)B\n"
			<< "B)C\n"
			<< "C)D\n"
			<< "D)E\n"
			<< "E)F\n"
			<< "B)G\n"
			<< "G)H\n"
			<< "D)I\n"
			<< "E)J\n"
			<< "J)K\n"
			<< "K)L\n";

	OrbitMap orbitMap;
	orbitMap.parseMap(mapData);
	std::cout << "test01: numObjects=" << orbitMap.numObjects() << "\n";
	auto checksum = orbitMap.calcOrbiterCountChecksum();
	assert(checksum == 42);
	std::cout << "test01: " << checksum << "\n";
}