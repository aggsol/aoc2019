#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <map>
#include <string>
#include <vector>

class IntCoder
{
public:
	IntCoder() = default;

	void parse(std::istream& is)
	{
		char c;
		std::string integer;
		while(is.get(c))
		{			
			if(c == ',')
			{
				assert(not integer.empty());
				auto val = std::stoi(integer);
				memory.push_back(val);
				integer.clear();
			}
			else
			{
				integer += c;
			}
		}

		if(not integer.empty())
		{
			auto val = std::stoi(integer);
			memory.push_back(val);
		}
	}

	void process()
	{
		bool isProcessing = true;
		int instrPtr = 0;
		while(isProcessing)
		{
			int opcode = memory.at(instrPtr);
			if(opcode == 1) // add
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				instrPtr++;
				auto b = memory.at(instrPtr);
				instrPtr++;
				auto dest = memory.at(instrPtr);

				memory.at(dest) = memory.at(a) + memory.at(b);
				instrPtr++;
			}
			else if(opcode == 2) // mul
			{
				instrPtr++;
				auto a = memory.at(instrPtr);
				instrPtr++;
				auto b = memory.at(instrPtr);
				instrPtr++;
				auto dest = memory.at(instrPtr);

				memory.at(dest) = memory.at(a) * memory.at(b);
				instrPtr++;
			}
			else if(opcode == 99) // halt
			{
				isProcessing = false;
			}
			else
			{
				std::cerr << "invalid opcode=" << opcode << "\n";
				return;
			}
		}
	}

	std::vector<int>	memory;
};

void runTest01()
{
	std::stringstream data;
	data << "1,0,0,0,99";

	IntCoder intcoder;
	intcoder.parse(data);

	assert(intcoder.memory.size() == 5);

	assert(intcoder.memory[0] == 1);
	assert(intcoder.memory[1] == 0);
	assert(intcoder.memory[2] == 0);
	assert(intcoder.memory[3] == 0);
	assert(intcoder.memory[4] == 99);

	intcoder.process();

	assert(intcoder.memory[0] == 2);
}

void runTest02()
{
	std::stringstream data;
	data << "2,3,0,3,99";

	IntCoder intcoder;
	intcoder.parse(data);
	intcoder.process();

	assert(intcoder.memory.size() == 5);

	assert(intcoder.memory[0] == 2);
	assert(intcoder.memory[1] == 3);
	assert(intcoder.memory[2] == 0);
	assert(intcoder.memory[3] == 6);
	assert(intcoder.memory[4] == 99);
}

int part1()
{
	// part 1
	std::ifstream input("input02.txt");

	if(not input.is_open())
	{
		std::cerr << "cannot open file.";
		return 100;
	}

	IntCoder intcoder;

	intcoder.parse(input);
	
	int noun = 12;
	int verb = 2;
	intcoder.memory[1] = noun;
	intcoder.memory[2] = verb;

	intcoder.process();

	std::cout << "part 1: memory[0]=" << intcoder.memory[0] << "\n";
	std::cout << "part 1: 100 * noun + verb=" << (100 * noun + verb) << "\n";
}

int main()
{
	runTest01();
	runTest02();

	part1();

	// part 2

	std::ifstream file("input02.txt");

	if(not file.is_open())
	{
		std::cerr << "cannot open file.";
		return 100;
	}
	std::stringstream buffer;
	buffer << file.rdbuf();
	auto data = buffer.str();

	for(int noun=0; noun<100; noun++)
	{
		for(int verb=0; verb<100; verb++)
		{
			std::stringstream is;
			is << data;

			IntCoder intcoder;
			intcoder.parse(is);

			assert(intcoder.memory.size() > 0);

			intcoder.memory[1] = noun;
			intcoder.memory[2] = verb;

			intcoder.process();

			if(intcoder.memory[0] == 19690720)
			{
				std::cout << "part 2: 100 * noun + verb=" << (100 * noun + verb) << "\n";
				return 0;
			}
		}
	}
	std::cerr << "Nothing found!\n";
	return 102;
}