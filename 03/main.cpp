#include <cassert>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

class Grid
{
public:
	Grid(int width, int height, int startX, int startY)
	: m_tiles(width*height)
	, m_width(width)
	, m_height(height)
	, m_startX(startX)
	, m_startY(startY)
	, m_currX(startX)
	, m_currY(startY)
	{
		std::cout << "Grid size [" << m_width << ", " << m_height << "]"
			<< " start [" << m_startX << ", " << m_startY << "]"
			<< "\n";
		for(int i=0; i<m_tiles.size(); ++i)
		{
			m_tiles.at(i) = '.';
		}		
		set(m_startX, m_startY, 'o');
	}

	void setCable(const std::string& cable)
	{
		m_currX = m_startX;
		m_currY = m_startY;

		std::string length;
		char dir = 0;
		int x = 0;
		int y = 0;
		for(int i=0; i<cable.length(); ++i)
		{
			auto curr = cable[i];
			if(curr == 'R' || curr == 'U' || curr == 'L' || curr == 'D')
			{
				dir = curr;
			}
			else if(curr == ',' || i == cable.length()-1)
			{
				if(curr != ',')
				{
					length += curr;
				}

				int val = std::stoi(length);
				length.clear();
				setCorner(m_currX, m_currY);
				if(dir == 'R')
				{
					for(int x = m_currX+1; x<=m_currX+val; ++x)
					{
						setCable(x, m_currY, '-');
					}
					m_currX += val;
				}
				else if(dir == 'L')
				{
					assert(val <= m_currX);
					for(int x = m_currX-1; x>=m_currX-val; --x)
					{
						setCable(x, m_currY, '-');
					}					
					m_currX -= val;
				}
				else if(dir == 'U')
				{
					for(int y = m_currY+1; y<=m_currY+val; ++y)
					{
						setCable(m_currX, y, '|');
					}
					m_currY += val;
				}
				else if(dir == 'D')
				{
					assert(val <= m_currY);
					for(int y = m_currY-1; y>=m_currY-val; --y)
					{
						setCable(m_currX, y, '|');
					}					
					m_currY -= val;
				}
				else
				{
					throw std::runtime_error("invalid dir");
				}
				dir = 0;
			}
			else
			{
				length += curr;
			}
		}	
	}

	void print()
	{		
		for(int l=m_height-1; l>=0; --l)
		{
			for(int c=0; c<m_width; ++c)
			{
				std::cout << m_tiles.at(l*m_width+c);
			}
			std::cout<< "\n";
		}
	}

	int findMinManhatten()
	{
		int minManhatten = calcManhatten(0, 0, m_width-1, m_height-1);
		int manhattenX = 1;
		int manhattenY = 2;
		for(int x=0; x<m_width; ++x)
		{
			for(int y=0; y<m_height; ++y)
			{
				char curr = get(x, y);
				if(curr != 'X') continue;
				auto val = calcManhatten(m_startX, m_startY, x, y);
				if(val < minManhatten) 
				{
					minManhatten = val;
					manhattenX = x;
					manhattenY = y;
				} 
			}
		}
		std::cout << "Min Manhatten Distance=" << minManhatten 
			<< " @ [" << manhattenX << ", " << manhattenY << "]"
			<< "\n";

		return minManhatten;
	}

	static int calcManhatten(int ax, int ay, int bx, int by)
	{
		return std::abs(ax-bx) + std::abs(ay-by);
	}

private:
	void setCorner(int x, int y)
	{
		if(x >= m_width || y >= m_height)
		{
			std::cerr << "setCorner x=" << x << " y=" << y
				<< " width=" << m_width << " height=" << m_height 
				<< "\n";
		}
		char curr = get(x, y);
		if(curr == 'X') return;
		if(curr == 'o') return;
		set(x, y, '+');
	}

	void setCable(int x, int y, char c)
	{
		if(x >= m_width || y >= m_height)
		{
			std::cerr << "setCable x=" << x << " y=" << y
				<< " width=" << m_width << " height=" << m_height 
				<< " c=" << c
				<< "\n";
		}
		char curr = get(x, y);
		assert(curr != 'o');
		if(curr == '-' || curr == '|') c = 'X';
		set(x, y, c);
	}

	void set(int x, int y, char c)
	{
		assert(x < m_width);
		assert(y < m_height);
		int index = y*m_width+x;
		m_tiles.at(index) = c;
	}

	char get(int x, int y)
	{
		assert(x < m_width);
		assert(y < m_height);
		int index = y*m_width+x;
		return m_tiles.at(index);
	}

	std::vector<char>	m_tiles;
	int			m_width = 0;
	int			m_height = 0;
	int			m_startX = 1;
	int			m_startY = 1;
	int			m_currX = 1;
	int			m_currY = 1;
};

int calcMaxExpansion(const std::string cable);
void example01();
void example02();
void example03();


int main()
{
	assert(Grid::calcManhatten(1, 1, 4, 4) == 6);
	
	//example01();
	//example02();
	//example03();
	
	std::ifstream file("input03.txt");
	if(not file.is_open())
	{
		std::cerr << "cannot open file\n";
		return 101;
	}

	std::string cable01;
	std::string cable02;
	std::getline(file, cable01);
	std::getline(file, cable02);

	auto dim01 = calcMaxExpansion(cable01);
	auto dim02 = calcMaxExpansion(cable02);

	auto max = std::max(dim01, dim02) + 24000;

	Grid grid(max, max, 12000, 12000);
	grid.setCable(cable01);
	grid.setCable(cable02);
	//grid.print();
	auto mm = grid.findMinManhatten();
	std::cout << "mm=" << mm << "\n";

	return 0;
}

void example01()
{
	std::string cable01("R8,U5,L5,D3");
	std::string cable02("U7,R6,D4,L4");

	auto dim01 = calcMaxExpansion(cable01);
	auto dim02 = calcMaxExpansion(cable02);

	auto max = std::max(dim01, dim02) + 3;

	Grid grid(max, max, 1, 1);
	grid.setCable(cable01);
	grid.setCable(cable02);
	grid.print();
	auto mm = grid.findMinManhatten();
	assert(mm == 6);
}

void example02()
{
	std::string cable01("R75,D30,R83,U83,L12,D49,R71,U7,L72");
	std::string cable02("U62,R66,U55,R34,D71,R55,D58,R83");

	auto dim01 = calcMaxExpansion(cable01);
	auto dim02 = calcMaxExpansion(cable02);

	auto max = std::max(dim01, dim02) + 60;
	std::cout << "max=" << max << "\n";

	Grid grid(max, max, 30, 30);
	grid.setCable(cable01);
	grid.setCable(cable02);
	//grid.print();
	auto minManhatten = grid.findMinManhatten();
	assert(minManhatten == 159);	
}

void example03()
{
	std::string cable01("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51");
	std::string cable02("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7");

	auto dim01 = calcMaxExpansion(cable01);
	auto dim02 = calcMaxExpansion(cable02);

	auto max = std::max(dim01, dim02) + 60;

	Grid grid(max, max, 30, 30);
	grid.setCable(cable01);
	grid.setCable(cable02);
	//grid.print();
	grid.findMinManhatten();
}

int calcMaxExpansion(const std::string cable)
{
	std::string length;	
	char dir = 0;
	int maxX = 0;
	int minX = 0;
	int maxY = 0;
	int minY = 0;
	int x = 0;
	int y = 0;
	for(int i=0; i<cable.length(); ++i)
	{
		auto curr = cable[i];
		if(curr == 'R' || curr == 'U' || curr == 'L' || curr == 'D')
		{
			dir = curr;
		}
		else if(curr == ',' || i == cable.length()-1)
		{
			if(curr != ',')
			{
				length += curr;
			}

			//std::cout << "lenght=" << length << "\n";
			int val = std::stoi(length);
			length.clear();
			if(dir == 'R')
			{
				x += val;
				if(x > maxX) maxX = x;
			}
			else if(dir == 'L')
			{
				x -= val;
				if(x < minX) minX = x;
			}
			else if(dir == 'U')
			{
				y += val;
				if(y > maxY) maxY = y;
			}
			else if(dir == 'D')
			{
				y -= val;
				if(y < minY) minY = y;
			}
			else
			{
				throw std::runtime_error("invalid dir");
			}
			dir = 0;
		}
		else
		{
			length += curr;
		}
	}	
	
	std::cout << "cable expansion: maxX=" << maxX << " minX=" << minX
	<< " maxY=" << maxY << " minY=" << minY << "\n";

	maxX += std::abs(minX);
	maxY += std::abs(minY);

	if(maxX > maxY) return maxX;

	return maxY;
}