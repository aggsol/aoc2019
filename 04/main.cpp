#include <cassert>
#include <iostream>
#include <string>
#include <vector>

bool isValidPart1(const std::string& val)
{
	if(val.size() != 6) return false;
	bool hasDoubleDigit = false;
	bool neverDecrease = true;

	char prev = 0;
	for(unsigned i=0; i<val.size(); ++i)
	{
		char curr = val[i];
		if(curr < prev) neverDecrease = false;
		if(curr == prev) hasDoubleDigit = true;
		prev = curr;
	}

	return hasDoubleDigit && neverDecrease;
}

bool isValid(const std::string& val)
{
	if(val.size() != 6) return false;
	bool hasDoubleDigit = false;
	bool neverDecrease = true;

	char prev = 0;
	unsigned clusterCount = 1;
	for(unsigned i=0; i<val.size(); ++i)
	{
		char curr = val[i];
		if(curr < prev)
		{ 
			neverDecrease = false;
			if(clusterCount == 2) hasDoubleDigit = true;
			clusterCount = 1;
		}
		else if(curr == prev) { 			
			clusterCount++;
			// special cas at the end
			if(clusterCount == 2 && i == val.size()-1) hasDoubleDigit = true;
		}
		else
		{
			if(clusterCount == 2) hasDoubleDigit = true;
			clusterCount = 1;					
		}
		prev = curr;
	}
	//std::cout << "hasDoubleDigit=" << hasDoubleDigit << " neverDecrease=" << neverDecrease << "\n";
	return hasDoubleDigit && neverDecrease;
}

int main()
{
	// Part 1 test
	assert(isValidPart1("111111"));
	assert(not isValidPart1("223450"));
	assert(not isValidPart1("123789"));

	// Part 2 test
	assert(isValid("112233"));
	assert(isValid("111133"));
	assert(not isValid("123444"));

	std::string start("147981");
	std::string end("691423");

	int startNum = 147981;
	int endNum = 691423;

	std::vector<std::string> candidates;
	for(int i=startNum; i<=endNum; ++i)
	{
		std::string val = std::to_string(i);
		if(isValid(val))
		{
			candidates.emplace_back(val);
		}
	}
	std::cout << "Part 2: Num of valid passwords: " << candidates.size() << "\n";
	return 0;

}